USE [DzialKredytow]
GO
/****** Object:  Table [dbo].[dict_Waluta]    Script Date: 10/04/2011 17:56:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dict_Waluta](
	[ID] [int] NOT NULL,
	[Waluta] [varchar](50) NOT NULL,
	[Symbol] [nvarchar](5) NOT NULL,
	[Skrot] [varchar](5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dict_RodzajeKredytow]    Script Date: 10/04/2011 17:56:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dict_RodzajeKredytow](
	[ID] [int] NOT NULL,
	[Rodzaj] [varchar](50) NOT NULL,
	[Oprocentowanie] [float] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dict_Dokument]    Script Date: 10/04/2011 17:56:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dict_Dokument](
	[ID] [int] NOT NULL,
	[Rodzaj] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AktualnaStopa]    Script Date: 10/04/2011 17:56:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AktualnaStopa](
	[ID] [int] NOT NULL,
	[DataZmiany] [datetime] NOT NULL,
	[Wartosc] [float] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Adres]    Script Date: 10/04/2011 17:56:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Adres](
	[ID] [int] NOT NULL,
	[Ulica] [varchar](30) NOT NULL,
	[NrDomu] [varchar](4) NOT NULL,
	[NrLokalu] [int] NULL,
	[KodPocztowy] [varchar](6) NOT NULL,
	[Miasto] [varchar](30) NOT NULL,
	[Kraj] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Zalacznik]    Script Date: 10/04/2011 17:56:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Zalacznik](
	[Id] [int] NOT NULL,
	[Nazwa] [varchar](50) NOT NULL,
	[Opis] [varchar](max) NULL,
	[Zawartosc] [varbinary](max) NULL,
	[DlugoscPliku] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Kredytobiorca]    Script Date: 10/04/2011 17:56:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Kredytobiorca](
	[ID] [int] NOT NULL,
	[Nazwisko] [varchar](100) NOT NULL,
	[Imie] [varchar](50) NOT NULL,
	[Pesel] [varchar](11) NOT NULL,
	[AdresId] [int] NOT NULL,
	[AdresKorespondencyjnyId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Kredyt]    Script Date: 10/04/2011 17:56:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kredyt](
	[KredytobiorcaId] [int] NULL,
	[RodzajId] [int] NULL,
	[WalutaId] [int] NULL,
	[Kwota] [float] NOT NULL,
	[IleRat] [int] NOT NULL,
	[DataZawarciaUmowy] [datetime] NULL,
	[DataSplatyPierwszejRaty] [datetime] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Kredyt] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DokumentKredytobiorcy]    Script Date: 10/04/2011 17:56:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DokumentKredytobiorcy](
	[DokumentId] [int] NULL,
	[KlientId] [int] NULL,
	[NrDokumentu] [varchar](50) NOT NULL,
	[Opis] [varchar](500) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ZalacznikKredytobiorcy]    Script Date: 10/04/2011 17:56:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZalacznikKredytobiorcy](
	[ZalacznikId] [int] NULL,
	[KredytobiorcaId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rata]    Script Date: 10/04/2011 17:56:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rata](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[KredytId] [int] NULL,
	[Kwota] [float] NULL,
	[DataWplaty] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK__DokumentK__Dokum__2E1BDC42]    Script Date: 10/04/2011 17:56:01 ******/
ALTER TABLE [dbo].[DokumentKredytobiorcy]  WITH CHECK ADD FOREIGN KEY([DokumentId])
REFERENCES [dbo].[dict_Dokument] ([ID])
GO
/****** Object:  ForeignKey [FK__DokumentK__Klien__2F10007B]    Script Date: 10/04/2011 17:56:01 ******/
ALTER TABLE [dbo].[DokumentKredytobiorcy]  WITH CHECK ADD FOREIGN KEY([KlientId])
REFERENCES [dbo].[Kredytobiorca] ([ID])
GO
/****** Object:  ForeignKey [FK__Kredyt__Kredytob__32E0915F]    Script Date: 10/04/2011 17:56:01 ******/
ALTER TABLE [dbo].[Kredyt]  WITH CHECK ADD FOREIGN KEY([KredytobiorcaId])
REFERENCES [dbo].[Kredytobiorca] ([ID])
GO
/****** Object:  ForeignKey [FK__Kredyt__RodzajId__33D4B598]    Script Date: 10/04/2011 17:56:01 ******/
ALTER TABLE [dbo].[Kredyt]  WITH CHECK ADD FOREIGN KEY([RodzajId])
REFERENCES [dbo].[dict_RodzajeKredytow] ([ID])
GO
/****** Object:  ForeignKey [FK__Kredyt__WalutaId__34C8D9D1]    Script Date: 10/04/2011 17:56:01 ******/
ALTER TABLE [dbo].[Kredyt]  WITH CHECK ADD FOREIGN KEY([WalutaId])
REFERENCES [dbo].[dict_Waluta] ([ID])
GO
/****** Object:  ForeignKey [FK__Kredytobi__Adres__2B3F6F97]    Script Date: 10/04/2011 17:56:01 ******/
ALTER TABLE [dbo].[Kredytobiorca]  WITH CHECK ADD FOREIGN KEY([AdresId])
REFERENCES [dbo].[Adres] ([ID])
GO
/****** Object:  ForeignKey [FK__Kredytobi__Adres__2C3393D0]    Script Date: 10/04/2011 17:56:01 ******/
ALTER TABLE [dbo].[Kredytobiorca]  WITH CHECK ADD FOREIGN KEY([AdresKorespondencyjnyId])
REFERENCES [dbo].[Adres] ([ID])
GO
/****** Object:  ForeignKey [FK__Rata__KredytId__4BAC3F29]    Script Date: 10/04/2011 17:56:01 ******/
ALTER TABLE [dbo].[Rata]  WITH CHECK ADD FOREIGN KEY([KredytId])
REFERENCES [dbo].[Kredyt] ([Id])
GO
/****** Object:  ForeignKey [FK__Zalacznik__Kredy__3B75D760]    Script Date: 10/04/2011 17:56:01 ******/
ALTER TABLE [dbo].[ZalacznikKredytobiorcy]  WITH CHECK ADD FOREIGN KEY([KredytobiorcaId])
REFERENCES [dbo].[Kredytobiorca] ([ID])
GO
/****** Object:  ForeignKey [FK__Zalacznik__Zalac__3A81B327]    Script Date: 10/04/2011 17:56:01 ******/
ALTER TABLE [dbo].[ZalacznikKredytobiorcy]  WITH CHECK ADD FOREIGN KEY([ZalacznikId])
REFERENCES [dbo].[Zalacznik] ([Id])
GO
