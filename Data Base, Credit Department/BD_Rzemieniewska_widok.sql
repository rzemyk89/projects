USE [DzialKredytow]
GO
/****** Object:  View [dbo].[UdzieloneKredyty]    Script Date: 10/04/2011 18:58:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UdzieloneKredyty]
AS
SELECT      rk.Rodzaj, 
			rk.Oprocentowanie, 
			k.IleRat, 
			k.Kwota AS KwotaKredytu, 
			dbo.fn_oblicz_rate(k.Id) * k.IleRat AS SumaZOprocentowaniem, 
			w.Skrot AS Waluta, 
			kb.Nazwisko, 
			kb.Imie, 
			SUM(CONVERT(float, r.Kwota)) AS Wplacil, 
			COUNT(r.Id) AS SplaconoRat, 
            dbo.fn_oblicz_rate_z_waluta(k.Id) AS Rata
FROM         dbo.Kredyt AS k INNER JOIN
                      dbo.Kredytobiorca AS kb ON kb.Id = k.KredytobiorcaId INNER JOIN
                      dbo.dict_RodzajeKredytow AS rk ON rk.Id = k.RodzajId INNER JOIN
                      dbo.dict_Waluta AS w ON w.Id = k.WalutaId LEFT OUTER JOIN
                      dbo.Rata AS r ON r.KredytId = k.Id
GROUP BY k.Id, rk.Rodzaj, rk.Oprocentowanie, k.Kwota, w.Skrot, kb.Nazwisko, kb.Imie, k.IleRat
GO
