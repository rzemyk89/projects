﻿USE [DzialKredytow]
GO
/****** Object:  StoredProcedure [dbo].[sp_wyswietl_harmonogram]    Script Date: 10/04/2011 18:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_wyswietl_harmonogram]
	@kredytId int
AS
BEGIN
	DECLARE @rata float;
	DECLARE	@harmonogram TABLE 
	( 
		id INT, 
		kwota VARCHAR(50), 
		dataSplaty Date
	)
	SET @rata = (dbo.fn_oblicz_rate(@kredytId));
	
	DECLARE @iloscRat INT;
	SET @iloscRat = (
	SELECT TOP 1 k.IleRat FROM Kredyt k WHERE k.Id = @KredytId)
	
	
	DECLARE @waluta VARCHAR(5);
	SET @waluta = (
	SELECT TOP 1 w.Symbol FROM Kredyt k
	JOIN dict_Waluta w ON w.Id = k.WalutaId
	WHERE k.Id = @KredytId)

	DECLARE @dataPierwszejRaty DATETIME;
	SET @dataPierwszejRaty = (
	SELECT TOP 1 k.DataSplatyPierwszejRaty FROM Kredyt k WHERE k.Id = @KredytId)
	
	DECLARE @i int;
	SET @i =1
	WHILE @i <= @iloscRat 
	BEGIN
	    INSERT INTO @harmonogram
		(id,kwota,dataSplaty)
		VALUES(@i,@rata,DATEADD(MONTH,@i-1,@dataPierwszejRaty))
		SET @i = @i + 1;
	END;

	SET NOCOUNT ON;
	SELECT h.id AS NrRaty, h.kwota + ' ' + @waluta AS Kwota, h.dataSplaty AS DataSplaty FROM @harmonogram h 
END
GO
