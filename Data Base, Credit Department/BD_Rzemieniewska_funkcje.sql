USE [DzialKredytow]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_pobierz_stopa_procentowa]    Script Date: 10/04/2011 18:26:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_pobierz_stopa_procentowa] 
(
)
RETURNS float
AS
BEGIN
	
    DECLARE @result  float;

	SET @result = (select top 1 s.Wartosc from AktualnaStopa s order by s.DataZmiany desc)
	RETURN @result

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_oblicz_rate]    Script Date: 10/04/2011 18:26:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_oblicz_rate] 
(
 @KredytId int
)
RETURNS FLOAT
AS
BEGIN
	
DECLARE @result  FLOAT;
DECLARE @oprocentowanie_rzeczywiste FLOAT;
DECLARE @sumaKredytu FLOAT;
DECLARE @iloscRat INT;

	SET @sumaKredytu = (
	SELECT TOP 1 k.Kwota FROM Kredyt k WHERE k.Id = @KredytId)

	SET @iloscRat = (
	SELECT TOP 1 k.IleRat FROM Kredyt k WHERE k.Id = @KredytId)

	SET @oprocentowanie_rzeczywiste = 
	((	dbo.fn_pobierz_stopa_procentowa()) 
	+
	(
		SELECT TOP 1 r.Oprocentowanie FROM Kredyt k 
		JOIN dict_RodzajeKredytow r ON r.Id = k.RodzajId 
		WHERE k.Id = @KredytId
	))/100;
	--rata = S * q^n * (q-1)/(q^n-1)
	-- q =  1 + (r/12)
	DECLARE @q FLOAT
	SET @q = 1 + (@oprocentowanie_rzeczywiste/12)
	
	SET @result =	(@sumaKredytu * POWER(@q,@iloscRat) *(@q-1) )
					/(POWER(@q,@iloscRat)-1)
	RETURN ROUND(@result,2)

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_oblicz_rate_z_waluta]    Script Date: 10/04/2011 18:26:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_oblicz_rate_z_waluta] 
(
 @KredytId int
)
RETURNS VARCHAR(10)
AS
BEGIN
	
DECLARE @waluta VARCHAR(5);
	SET @waluta = (
	SELECT TOP 1 w.Symbol FROM Kredyt k
	JOIN dict_Waluta w ON w.Id = k.WalutaId
	WHERE k.Id = @KredytId)
	
	RETURN CONVERT(VARCHAR(10),[dbo].fn_oblicz_rate(@KredytId)) + ' ' +@waluta

END
GO
