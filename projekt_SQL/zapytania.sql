﻿--zapytanie ukazujące główne informacje o kredycie danego klienta

select k.id_klienta, k.imie, k.nazwisko, t.typ_kredytu,t.nazwa,t.typ_oprocentowania,t.wibor3m,kk.nr_kredytu,kk.kwota_kredytu, kk.kapital,kk.odsetki,
case 
	when t.wibor3m='t' 
		then kk.oprocentowanie + (select wartosc 
						from wibor3m
						ORDER BY data DESC
						LIMIT 1)
	else kk.oprocentowanie
		end as oprocentowanie,
kk.okres_kredytu,wysokosc_raty(kk.nr_kredytu)
from klient k
	right join kredyty kk on k.id_klienta=kk.id_klienta --eliminujemy osoby bez kredytów
	left join typ_kredyt t on kk.id_typ=t.id_typ;



--zapytanie ukazujące ilość kredytów na klienta, łączną sumę kredytów oraz łączną ratę do spłaty 

select k.id_klienta, k.imie, k.nazwisko, count(kk.nr_kredytu) ilosc_kredytow, sum(kk.kwota_kredytu) laczna_kwota_kredytow, sum(kk.kapital) laczny_kapital,sum(kk.odsetki) laczne_odsetki,
max(kk.okres_kredytu), sum(wysokosc_raty(kk.nr_kredytu)) laczna_rata
from klient k
	left join kredyty kk on k.id_klienta=kk.id_klienta
group by k.id_klienta, k.imie, k.nazwisko
order by id_klienta ASC;


--Średni wiek klienta z kredytem mieszkaniowym

select AVG(EXTRACT(YEAR FROM current_date)-cast(case 
						when cast(substring(k.pesel from 3 for 2) as integer) <= 12 
							then '19' || left(k.pesel,2) 
						else '20' || left(k.pesel,2)
						end as integer)) wiek_kredytobiorcy, 	-- obecny rok - rok urodzenia klienta
											-- rok urodzenia dobry dla ludzi urodzonych po 1900 roku
											-- dzieci urodzone po 2000 roku maja +20 do miesiąca
t.typ_kredytu,t.nazwa
from klient k
	right join kredyty kk on k.id_klienta=kk.id_klienta --eliminujemy osoby bez kredytów
	left join typ_kredyt t on kk.id_typ=t.id_typ
group by t.typ_kredytu, t.nazwa
having t.typ_kredytu like '%mieszkaniowy';

--harmonogram spłat rat klientów oraz ich stan zadluzenia za pomocą funkcji


select * from harmonogram;

--suma rat z harmonogramu, pokazuje ile klient musi faktycznie splacić

select nr_kredytu, count(rata) ilosc_rat, sum(rata) suma_rat
from harmonogram
group by nr_kredytu;


--zapytanie sprawdzające ile aktualnie pozostało rat do całkowitej spłaty kredytu

select nr_kredytu, count(rata) ilość_rat_pozostalych_do_splaty, sum(rata) suma_rat_pozostalych_do_splaty
from harmonogram
where data > current_date
group by nr_kredytu
order by nr_kredytu ASC;






