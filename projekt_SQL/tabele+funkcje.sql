﻿--Utwórz wszystkie tabele w swoim projekcie bazy danych oraz wypełnij je danymi (min. 3 wiersze w każdej tabeli). 
--Skrypt z min. 8 zapytaniami (różnej trudności; min. 3 zapytania wykorzystujące łączenie tabel). Całość (3 skrypty - 1 zip) prześlij na moodle.


--tworzenie tabel
DROP TABLE IF EXISTS adres, klient, typ_kredyt, kredyty, harmonogram, wibor3m;

CREATE TABLE adres (
	id_klienta integer, --SERIAL, --autoinkrementacja
	ulica varchar(100),
	nr_domu varchar(5),
	nr_mieszkania integer,
	kod_pocztowy varchar(6),
	miasto varchar(45),
	PRIMARY KEY(id_klienta)
);

CREATE TABLE klient ( 
	id_klienta integer REFERENCES adres,
	imie varchar(20),
	nazwisko varchar(50),
	pesel varchar(11),
	nr_dowodu varchar(9),
	telefon integer
	
);

CREATE TABLE typ_kredyt (
	id_typ integer, --SERIAL,
	typ_kredytu varchar(20),
	nazwa varchar(45),
	typ_oprocentowania varchar(7),
	wibor3m boolean,
	PRIMARY KEY(id_typ)
);

CREATE TABLE kredyty (
	nr_kredytu integer, --SERIAL,
	id_klienta integer REFERENCES adres,
	id_typ integer REFERENCES typ_kredyt,
	data_umowy date,
	data_pierwszej_raty date,
	kwota_kredytu integer,
	kapital integer,
	odsetki float,
	oprocentowanie float,
	okres_kredytu integer,
	PRIMARY KEY(nr_kredytu)
);


CREATE TABLE harmonogram (
	nr_kredytu integer REFERENCES kredyty,
	nr_raty integer,
	data date,
	rata FLOAT,
	stan_zadluzenia float,
	UNIQUE (nr_kredytu,nr_raty,data,rata,stan_zadluzenia)
);

CREATE TABLE wibor3m (
	data date,
	wartosc float
);

--tworzenie funkcji

DROP FUNCTION IF EXISTS walidacja_pesel();
DROP FUNCTION IF EXISTS wysokosc_raty();
DROP FUNCTION IF EXISTS harmonogram();


--funkcja sprawdzająca, czy podany numer pesel jest poprawny
CREATE OR REPLACE FUNCTION walidacja_pesel() 
RETURNS trigger AS 
$BODY$

	BEGIN
		IF 

		--new.pesel='22'
		cast(substring(new.pesel from 11 for 1) as integer) =(9*cast(substring(new.pesel from 1 for 1) as integer) + 7*cast(substring(new.pesel from 2 for 1) as integer) + 
		3*cast(substring(new.pesel from 3 for 1) as integer) + 1*cast(substring(new.pesel from 4 for 1) as integer) + 9*cast(substring(new.pesel from 5 for 1)as integer) + 
		7*cast(substring(new.pesel from 6 for 1)as integer)+ 3*cast(substring(new.pesel from 7 for 1)as integer)+ 1*cast(substring(new.pesel from 8 for 1)as integer) + 
		9*cast(substring(new.pesel from 9 for 1) as integer) + 7*cast(substring(new.pesel from 10 for 1)as integer))%10  

		THEN
		RETURN new;
		ELSE 
		RAISE EXCEPTION 'pesel nieprawidlowy';
		END IF;
	END 
$BODY$
LANGUAGE 'plpgsql' IMMUTABLE;

CREATE TRIGGER walidacja
BEFORE INSERT OR UPDATE ON klient
FOR EACH ROW
EXECUTE PROCEDURE walidacja_pesel();



--funkcja wyliczająca wysokość raty stałej
CREATE OR REPLACE FUNCTION wysokosc_raty(nr_kredyt integer) RETURNS FLOAT AS
$BODY$
	DECLARE
		S integer := (select kwota_kredytu from kredyty k where k.nr_kredytu=nr_kredyt);
		n integer :=(select okres_kredytu from kredyty k where k.nr_kredytu=nr_kredyt);
		r float :=(
			select case when t.wibor3m='t' then k.oprocentowanie + (select wartosc from wibor3m where data=CURRENT_DATE)
				else oprocentowanie
				end as nowosc
			from kredyty k
				left join typ_kredyt t on k.id_typ=t.id_typ
			where k.nr_kredytu=nr_kredyt
				);
	BEGIN
		
		RETURN round(cast(S*((1 + r/12)^n)*(((1 + r/12)-1)/(((1 + r/12)^n)-1)) as numeric),2);		
--q = 1 + r/m

--Oznaczenia:
--S – kwota zaciągniętego kredytu (kwota brutto, wraz z kosztami okołokredytowymi)
--n – ilość rat
--r – oprocentowanie nominalne dla okresu
--m – ilość rat w okresie (najczesciej w roku), na sztywno 12
	END
$BODY$
LANGUAGE 'plpgsql' IMMUTABLE;


--funkcja, za pomocą której można uzupełnić tabelę harmonogram
CREATE OR REPLACE FUNCTION harmonogram(nr_kredyt integer) RETURNS TABLE(id integer, kwota float, DataSplaty date) AS
$BODY$

		DECLARE rata FLOAT:= wysokosc_raty(nr_kredyt);
			iloscRat INTEGER := (SELECT okres_kredytu FROM kredyty k WHERE k.nr_kredytu = nr_kredyt);
			dataPierwszejRaty DATE := (SELECT data_pierwszej_raty FROM kredyty k WHERE k.nr_kredytu = nr_kredyt);
			kapital integer := (select kapital from kredyty k WHERE k.nr_kredytu = nr_kredyt);
			odsetki float := (select odsetki from kredyty k WHERE k.nr_kredytu = nr_kredyt);
		
			i INTEGER := 1;
		BEGIN
			WHILE i <= iloscRat LOOP
					INSERT INTO harmonogram VALUES(nr_kredyt, i,(dataPierwszejRaty+ INTERVAL '1 month' * (i-1)),rata,kapital+odsetki-i*rata);
				i := i + 1;   
			END LOOP;
		END;

$BODY$
LANGUAGE 'plpgsql' volatile;

			
