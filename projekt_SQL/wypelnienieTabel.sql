﻿-- wypełnienie tabeli adres
--(id_klienta,ulica,nr_domu,nr_mieszkania,kod_pocztowy,miasto)
INSERT INTO adres(id_klienta,ulica,nr_domu,nr_mieszkania,kod_pocztowy,miasto) 
VALUES
	(1,'Grunwaldzka','123a',50, '80-111','Gdańsk'),
	(2,'Legionów','23',12,'82-300','Elbląg'),
	(3,'Grzybowa','8d',NULL,'80-298','Gdańsk'),
	(4,'Iwaszkiewicza','89c','23','10-089','Olsztyn'),
	(5,'Legionów','23',12,'82-300','Elbląg');

--wypełnieni tabeli klient
--(id_klienta,imie,nazwisko,pesel,nr_dowodu,telefon)
INSERT INTO klient VALUES(1,'Jan','Kowalski','78042813119','ANL422798',508808808),
			 (2,'Adam','Nowak','91072618693','AMG234629',956012345),
			 (3,'Ewa','Mamzer','74082404726','ACR898523',123456789),
			 (4,'Iwona','Saj','58112518047','ATV616207',987654321),
			 (5,'Hanna','Mostowiak','58112518047','AMN784512',560288999);

--wypełnienie tabeli typ_kredyt
--(id_typ,typ_kredytu,nazwa,typ_oprocentowania,wibor3m)
INSERT INTO typ_kredyt(id_typ,typ_kredytu,nazwa,typ_oprocentowania,wibor3m) 
VALUES
	(1,'samochodowy','moje wymarzone autko','stałe',FALSE),
	(2,'mieszkaniowy','mój własny kącik','zmienne', TRUE),
	(3,'mieszkaniowy','moje wymarzone M','zmienne', TRUE),
	(4,'gotówkowy','dowolny cel','stałe', FALSE);

--wypełnienie tabeli kredyty
--nr_kredytu,id_klienta,id_typ,data_umowy, data_pierwszej_raty,kwota_kredytu,kapital,odsetki,oprocentowanie,okres_kredytu
INSERT INTO kredyty(nr_kredytu,id_klienta,id_typ,data_umowy, data_pierwszej_raty,kwota_kredytu,kapital,odsetki,oprocentowanie,okres_kredytu) 
VALUES
	(1,1,1,'05 Dec 2015','10 Dec 2015',20000,20000,2231.51,0.07,36),
	(2,2,3,'10 Dec 2016','15 Dec 2016',150000,150000,56499.6,0.05,120),
	(3,2,2,'25 Dec 2013','30 Dec 2013',300000,300000,328887.6,0.04,360),
	(4,4,4,'17 Dec 2016','28 Dec 2016',15000,15000,1612.08,0.1,24),
	(5,4,2,'17 Nov 2014','28 Nov 2014',200000,200000,136451.2,0.04,240);

--wypełnienie tabeli wibor3m
--data,wartosc
INSERT INTO wibor3m VALUES
	('11 Jan 2017',0.0173),
	('10 Jan 2017',0.0173),
	('09 Jan 2017',0.0173),
	('06 Jan 2017',0.0173),
	('05 Jan 2017',0.0173),
	('04 Jan 2017',0.0173),
	('03 Jan 2017',0.0173),
	('02 Jan 2017',0.0173),
	('31 Dec 2016',0.0173),
	('30 Dec 2016',0.0173),
	('29 Dec 2016',0.0173),
	('28 Dec 2016',0.0173),
	('27 Dec 2016',0.0173),
	('23 Dec 2016',0.0173);

--wypelnienie tabeli harmonogram (należy wcześniej wykonać zapytania z funkcjami)
select harmonogram(1);
select harmonogram(2);
select harmonogram(3);
select harmonogram(4);
select harmonogram(5);