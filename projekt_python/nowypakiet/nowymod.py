# -*- coding: utf-8 -*-
## @package nowymod
#  Modul zawierajacy definicje podstawowych klas oraz metod.
#
#  importuje sqlite3 

import sqlite3

## Sciezka polaczen do bazy danych
#
db_path = 'piwko.db'

## Wyjątek używany w repozytorium
#
class RepositoryException(Exception):
    ## Konstruktor. ##
    #  @param self Wskaznik obiektu.
    #  @param message Wiadomosc.
    #  @param *errors Lista bledow.
    def __init__(self, message, *errors):
        Exception.__init__(self, message)
	## Zmienna przechowujaca bledy.
        self.errors = errors


## Model danych Grupy piwowarskiej
#
class GrupaPiwowarska():
  

    ## Konstruktor. ##
    #  @param self Wskaznik obiektu.
    #  @param id ID grupy piwowarskiej.
    #  @param nazwa Nazwa grupy piwowarskiej.
    #  @param browary Lista Browarow nalezacych do grupy.
    def __init__(self, id, nazwa="", browary=[]):
	## Kolumna w bazie danych przechowujaca ID grupy piwowarskiej.
        self.GID = id
	## Kolumna w bazie danych przechowujaca nazwe grupy piwowarskiej.
        self.nazwa = nazwa
	## Zmienna przechowujaca liste browarow przynaleznych do grupy piwowarskiej.
        self.browary = browary
        
    ## Reprezentator. ##
    #  @param self Wskaznik obiektu.
    #
    #  zwaraca : <GrupaPiwowarska(gid='%s', nazwa='%s',browary='%s')>
                )     
    def __repr__(self):
        return "<GrupaPiwowarska(gid='%s', nazwa='%s',browary='%s')>" % (
                    self.GID, self.nazwa, self.browary
                )
## Model pojedynczego browaru w grupie piwowarskiej.
#
#  Występuje tylko wewnątrz obiektu GrupyPiwowarskie.
class Browar():
    
    ## Konstruktor. ##
    #  @param self Wskaznik obiektu.
    #  @param id ID browaru.
    #  @param nazwa Nazwa browaru.
    #  @param miasto Miasto w ktorym znajduje sie browar.
    #  @param rok_zalozenia Rok zalozenia browaru.
    #  @param piwka Marki piw produkowanych przez browar.
    def __init__(self, id, nazwa, miasto,rok_zalozenia, piwka=[]):
        ## Kolumna w bazie danych przechowujaca ID browaru.
        self.BID = id
	## Kolumna w bazie danych przechowujaca Nazwe browaru.
        self.nazwa = nazwa
	## Kolumna w bazie danych przechowujaca miasto browaru.
        self.miasto = miasto
	## Kolumna w bazie danych przechowujaca rok zalozenia browaru.
        self.rok_zalozenia = rok_zalozenia
	## Zmienna przechowujaca liste piwek produkowanych przez browar.
        self.piwka = piwka


    ## Reprezentator. ##
    #  @param self Wskaznik obiektu.
    #
    #  zwaraca : <Browar(bid='%s', nazwa='%s', miasto='%s',rok_zalozenia='%s',piwka='%s')>
    def __repr__(self):
        return "<Browar(bid='%s', nazwa='%s', miasto='%s',rok_zalozenia='%s',piwka='%s')>" % (
                    self.BID, self.nazwa, self.miasto, self.rok_zalozenia, self.piwka
                    )

## Model piwka w browarze. 
#
# Występuje tylko wewnątrz obiektu Browar, a co za tym idzie w wobiekcie GrupyPiwowarskie.
class Piwko():
    ## Konstruktor. ##
    #  @param self Wskaznik obiektu.
    #  @param id ID piwka.
    #  @param nazwa nazwa piwka.
    #  @param typ_piwa Typ Piwa.
    #  @param procenty Zawartosc alkoholu etylowego.
    def __init__(self, id, nazwa, typ_piwa,procenty):
	## Kolumna w bazie danych przechowujaca ID piwka.
        self.PID = id
	## Kolumna w bazie danych przechowujaca nazwe piwka.
        self.nazwa = nazwa
	## Kolumna w bazie danych przechowujaca typ piwka.
        self.typ_piwa = typ_piwa
	## Kolumna w bazie danych przechowujaca moc piwka.        
	self.procenty = procenty

    ## Reprezentator. ##
    #  @param self Wskaznik obiektu.
    #
    #  zwaraca: <Piwko(pid='%s', nazwa='%s', typ_piwa='%s',procenty='%s')>
    def __repr__(self):
        return "<Piwko(pid='%s', nazwa='%s', typ_piwa='%s',procenty='%s')>" % (
                    self.PID, self.nazwa, self.typ_piwa, str(self.procenty) 
                    )


## Klasa bazowa repozytorium
#
class Repository():
    ## Konstruktor. ##
    #  @param self Wskaznik obiektu.
    def __init__(self):
        try:
            ## Zmienna przechowujaca polaczenie do bazy danych.
            self.conn = self.get_connection()
        except Exception as e:
            raise RepositoryException('GET CONNECTION:', *e.args)
        self._complete = False

    ## wejście do with ... as ...
    #  @param self Wskaznik obiektu.
    def __enter__(self):
        return self

    ## wyjście z with ... as ...
    #  @param self Wskaznik obiektu.
    #  @param type_ Typ.
    #  @param value Wartosc.
    #  @param traceback Traceback.
    def __exit__(self, type_, value, traceback):
        self.close()

    ## Metoda przechowuje status polaczenia z baza danych
    def complete(self):
        self._complete = True

    ## Zwroc polaczenie do bazy danych
    #  @param self Wskaznik obiektu.
    def get_connection(self):
        return sqlite3.connect(db_path)

    ## Zamknij polaczenie z baza 
    #  @param self Wskaznik obiektu.
    def close(self):
        if self.conn:
            try:
                if self._complete:
                    self.conn.commit()
                else:
                    self.conn.rollback()
            except Exception as e:
                raise RepositoryException(*e.args)
            finally:
                try:
                    self.conn.close()
                except Exception as e:
                    raise RepositoryException(*e.args)


## Repozytorium obiektow typu GrupaPiwowarska
#
# Agreguje klasy Browar i Piwko
class GrupaPiwowarskaRepository(Repository):

    ## Metoda dodaje pojedynczą grupe piwowarska do bazy danych, wraz ze wszystkimi jej pozycjami.
    #  @param self Wskaznik obiektu.
    #  @param GrupaPiwowarska Obiekt klasy GrupaPiwowarska.
    def add(self, GrupaPiwowarska):

        try:
            c = self.conn.cursor()
            # zapisz nagłowek grupy
            c.execute('INSERT INTO GrupyPiwowarskie (GID, nazwa) VALUES(?, ?)',
                        (GrupaPiwowarska.GID, GrupaPiwowarska.nazwa)
                    )
            # zapisz pozycje browaru w grupie
            if GrupaPiwowarska.browary:
                for browar in GrupaPiwowarska.browary:
                    try:
                        c.execute('INSERT INTO Browary (GID,BID,nazwa,miasto,rok_zalozenia) VALUES(?,?,?,?,?)',
                                        (GrupaPiwowarska.GID, browar.BID, browar.nazwa, browar.miasto, browar.rok_zalozenia)
                                )
                    except Exception as e:
                        print "item add error:", e
                        raise RepositoryException('error adding browar: %s, to grupa piwowarska: %s' %
                                                    (str(browar), str(GrupaPiwowarska.GID))
                                                )
            # zapisz pozycje piwka w browarze
                    for piwko in browar.piwka: 
                        try:
                            c.execute('INSERT INTO Piwka (BID,PID,nazwa,typ_piwa,procenty) VALUES(?,?,?,?,?)',
                                            (browar.BID, piwko.PID,piwko.nazwa, piwko.typ_piwa, str(piwko.procenty))
                                    )
                        except Exception as e:
                            print "item add error:", e
                            raise RepositoryException('error adding piwko: %s, to browar: %s' %
                                                        (str(piwko), str(browar.BID))
                                                    )
        except Exception as e:
            print "browar/piwko add error:", e
            raise RepositoryException('error adding grupa piwowarska %s' % str(GrupaPiwowarska))

    ## Metoda usuwa pojedynczą grupe piwowarska do bazy danych, wraz ze wszystkimi jej pozycjami.
    #  @param self Wskaznik obiektu.
    #  @param GrupaPiwowarska Obiekt klasy GrupaPiwowarska.
    def delete(self, GrupaPiwowarska):    
        try:
            c = self.conn.cursor()
            # usuń pozycje (piwka i browary)
            c.execute('''
                    delete from piwka
                    where (select b.GID from piwka p
                            left join browary b on p.BID=b.BID)=?

            ''', (GrupaPiwowarska.GID,))
            c.execute('DELETE FROM Browary WHERE GID=?', (GrupaPiwowarska.GID,))
            # usuń nagłowek (grupa piwowarska)
            c.execute('DELETE FROM GrupyPiwowarskie WHERE GID=?', (GrupaPiwowarska.GID,))

        except Exception as e:
            print "GrupaPiwowarska delete error:", e
            raise RepositoryException('error deleting grupa piwowarska %s' % str(GrupaPiwowarska))

    ## Metoda zwraca pojedynczą grupe piwowarska z bazy danych, obiekt zawiera przynalezne browary i piwka.
    #  @param self Wskaznik obiektu.
    #  @param id ID rekordu bazy danych.
    def getById(self, id):
        try:
            c = self.conn.cursor()
            c.execute("SELECT * FROM GrupyPiwowarskie WHERE GID=?", (id,))
            gr_row = c.fetchone()

            grupaPiwowarska=None

            if gr_row is not None:
                grupaPiwowarska = GrupaPiwowarska(id)    
            
                grupaPiwowarska.nazwa = gr_row[1]
                c.execute("SELECT * FROM Browary WHERE GID=? order by nazwa", (id,))
                gr_br_rows = c.fetchall()
                
                br_list = []
                for br_row in gr_br_rows:

                    c.execute('''
                            SELECT * FROM Piwka
                            WHERE BID=?
                            order by nazwa
                        ''', (br_row[1],))
                    p_br_rows = c.fetchall()

                    p_list=[]
                    for p_row in p_br_rows:
                        p = Piwko(id=p_row[1], nazwa=p_row[2], typ_piwa=p_row[3], procenty=p_row[4])
                        p_list.append(p)

                
                    br = Browar(id=br_row[1], nazwa=br_row[2], miasto=br_row[3], rok_zalozenia=br_row[4],piwka=p_list)
                    br_list.append(br)
                grupaPiwowarska.browary=br_list

       
        except Exception as e:
            print "grupa piwowarska getById error:", e
            raise RepositoryException('error getting by id grupa piwowarska id: %s' % str(id))
        return grupaPiwowarska

    ## Metoda uaktualnia pojedynczą grupe piwowarska w bazie danych, wraz ze wszystkimi jej pozycjami
    #  @param self Wskaznik obiektu.
    #  @param grupaPiwowarska Obiekt klasy GrupaPiwowarska.
    def update(self, grupaPiwowarska):
        try:
            # pobierz z bazy fakturę
            gr_oryg = self.getById(grupaPiwowarska.GID)
            if gr_oryg != None:
                # grupa jest w bazie: usuń ją
                self.delete(grupaPiwowarska)
            self.add(grupaPiwowarska)

        except Exception as e:
            print "GrupaPiwowarska update error:", e
            raise RepositoryException('error updating grupa piwowarska %s' % str(grupaPiwowarska))

#if __name__ == "__main__":
#    test(__file__)
