# -*- coding: utf-8 -*-

import repository
import sqlite3
import unittest

db_path = 'piwko.db'

## Klasa testujaca klase Repository z modulu nowymod. 
#
class RepositoryTest(unittest.TestCase):
    ## Metoda czysci baze danych i dodaje wpisy na potrzeby testow
    #  @param self Wskaznik obiektu.
    def setUp(self):
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute('DELETE FROM GrupyPiwowarskie')
        c.execute('DELETE FROM Browary')
        c.execute('DELETE FROM Piwka')
        c.execute('''INSERT INTO GrupyPiwowarskie (GID, nazwa) VALUES(1, 'Grupa Zywiec')''')
        c.execute('''INSERT INTO Browary (GID,BID,nazwa, miasto, rok_zalozenia)
                    VALUES(1,1,'Browar Bracki','Cieszyn','1846')''')
        c.execute('''INSERT INTO Piwka (BID, PID, nazwa, typ_piwa, procenty)
                    VALUES(1,1,'Brackie','jasne pelne',0.055)''')
        conn.commit()
        conn.close()

    ## Metoda usuwa dane z bazy danych po zakonczeniu testow.
    # \test Usun wpisy z tabel: GrupyPiwowarskie, Browary, Piwka
    #  @param self Wskaznik obiektu.
    def tearDown(self):
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute('DELETE FROM GrupyPiwowarskie')
        c.execute('DELETE FROM Browary')
        c.execute('DELETE FROM Piwka')
        conn.commit()
        conn.close()

    ## Metoda testuje funkcjonalnosc metody getById() 
    # \test Sprawdz czy metoda repository.GrupaPiwowarskaRepository().getById() zwraca obiekt klasy repository.GrupaPiwowarska w przypadku gdy rekord istnieje.
    #  @param self Wskaznik obiektu.
    def testGetByIdInstance(self):
        grupaPiwowarska = repository.GrupaPiwowarskaRepository().getById(1)
        errorString = "Obiekt nie jest klasa GrupaPiwowarska"
        self.assertIsInstance(grupaPiwowarska, repository.GrupaPiwowarska, errorString)

    ## Metoda testuje funkcjonalnosc metody getById() 
    # \test Sprawdz czy metoda repository.GrupaPiwowarskaRepository().getById() zwraca obiekt klasy None w przypadku gdy rekord istnieje.
    #  @param self Wskaznik obiektu.
    def testGetByIdNotFound(self):
        testValue=repository.GrupaPiwowarskaRepository().getById(22)
        expectedValue = None
        errorString = "Powinno wyjść None"
        self.assertEqual(testValue,expectedValue, errorString )
    ## Metoda testuje funkcjonalnosc metody getById() 
    # \test Sprawdz czy metoda repository.GrupaPiwowarskaRepository().getById() zwraca oczekiwana ilosc obiektow. 
    #  @param self Wskaznik obiektu.
    def testGetByIdBrowaryLen(self):
        testValue = len(repository.GrupaPiwowarskaRepository().getById(1).browary)
        expectedValue = 1
        errorString = "Powinno wyjść %i, wyszlo: %i" %(expectedValue,testValue)
        self.assertEqual(testValue,expectedValue, errorString)


if __name__ == "__main__":
    unittest.main()
