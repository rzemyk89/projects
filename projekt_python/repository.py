# -*- coding: utf-8 -*-

## @package nowymod
#  Modul glowny programu, zawiera funkcje main.
#
#  importuje nowypakiet.nowymod 

from nowypakiet.nowymod import *

if __name__ == '__main__':
    try:
        with GrupaPiwowarskaRepository() as GrupaPiwowarska_repository:
            GrupaPiwowarska_repository.add(
                GrupaPiwowarska(id = 1, nazwa="Grupa Zywiec",
                        browary = [
                            Browar(id= 1, nazwa = "Browar Bracki",   miasto = "Cieszyn", rok_zalozenia = "1846",
                                   piwka = [Piwko(id=1, nazwa="Brackie", typ_piwa="jasne pelne", procenty =0.055)]),
                            Browar(id= 2, nazwa = "Browar Elblag",   miasto = "Elblag", rok_zalozenia = "1872",
                                   piwka = [Piwko(id=2, nazwa="EB", typ_piwa="jasne pelne", procenty =0.06)]),
                            Browar(id= 3, nazwa = "Browar Lezajsk",   miasto = "Stare Miasto", rok_zalozenia = "1978",
                                   piwka = [Piwko(id=3, nazwa="Lezajsk", typ_piwa="pelne", procenty =0.055)])
                        ]
                    )
                )
            GrupaPiwowarska_repository.complete()
    except RepositoryException as e:
        print(e)

    print GrupaPiwowarskaRepository().getById(1)

    try:
        with GrupaPiwowarskaRepository() as GrupaPiwowarska_repository:
            GrupaPiwowarska_repository.update(
                GrupaPiwowarska(id = 1, nazwa="Grupa Zywiec",
                        browary = [
                            Browar(id= 1, nazwa = "Browar Bracki",   miasto = "Cieszyn", rok_zalozenia = "1846",
                                   piwka = [Piwko(id=1, nazwa="Zywiec Porter", typ_piwa="ciemne", procenty =0.06)])#,
                            #Browar(id= 4, nazwa = "Browar Warka",   miasto = "Warka", rok_zalozenia = "1975"),
                            #Browar(id= 5, nazwa = "Browar Zywiec",   miasto = "Zywiec", rok_zalozenia = "1856"),

                        ]
                    )
                )
            GrupaPiwowarska_repository.complete()
    except RepositoryException as e:
        print(e)

    print GrupaPiwowarskaRepository().getById(1)

  
