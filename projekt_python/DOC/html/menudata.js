var menudata={children:[
{text:"Strona główna",url:"index.html"},
{text:"Dodatkowe strony",url:"pages.html"},
{text:"Pakiety",url:"namespaces.html",children:[
{text:"Pakiety",url:"namespaces.html"}]},
{text:"Klasy",url:"annotated.html",children:[
{text:"Lista klas",url:"annotated.html"},
{text:"Indeks klas",url:"classes.html"},
{text:"Hierarchia klas",url:"hierarchy.html"},
{text:"Składowe klas",url:"functions.html",children:[
{text:"Wszystko",url:"functions.html",children:[
{text:"_",url:"functions.html#index__"},
{text:"a",url:"functions.html#index_a"},
{text:"b",url:"functions.html#index_b"},
{text:"c",url:"functions.html#index_c"},
{text:"d",url:"functions.html#index_d"},
{text:"e",url:"functions.html#index_e"},
{text:"g",url:"functions.html#index_g"},
{text:"m",url:"functions.html#index_m"},
{text:"n",url:"functions.html#index_n"},
{text:"p",url:"functions.html#index_p"},
{text:"r",url:"functions.html#index_r"},
{text:"s",url:"functions.html#index_s"},
{text:"t",url:"functions.html#index_t"},
{text:"u",url:"functions.html#index_u"}]},
{text:"Funkcje",url:"functions_func.html"},
{text:"Zmienne",url:"functions_vars.html"}]}]}]}
