#W oparciu o prezentowaną implementacje wzorca repozytorium:

#Zaprojektuj własny model danych z najmniej jedną relacją 1-wiele. (10%)
#Zaimplementuj dla modelu z p.1. w repozytorium z operacjami:
#    standard: Add, Delete, Update, GetBy... (30%)
#    wyniki operacji omawianych w części Statystyka w Pythonie (30%)
#Do wykonanego repozytorium stwórz testy w oparciu o bibliotekę unittest, by przetestować podstawową funkcjonalność repozytorium. (20%)
#Umieść kod w repozytorium kodu, np.: github.com i udostępnij prowadzącemu. (10%)

import sqlite3


db_path = 'piwko.db'
conn = sqlite3.connect(db_path)

c = conn.cursor()
#
# Tabele
#
c.execute("DROP TABLE IF EXISTS GrupyPiwowarskie")
c.execute('''
    CREATE TABLE GrupyPiwowarskie
        (GID INTEGER PRIMARY KEY,
        nazwa varchar(30))
          ''')
c.execute("DROP TABLE IF EXISTS Browary")
c.execute('''
    CREATE TABLE Browary
        (GID INTEGER,
        BID INTEGER,
        nazwa varchar(30),
        miasto varchar(30),
        rok_zalozenia varchar(4),
        FOREIGN KEY (GID) REFERENCES GrupyPiwowarskie(GID),
        PRIMARY KEY (BID))
          ''')
c.execute("DROP TABLE IF EXISTS Piwka")
c.execute('''
    CREATE TABLE Piwka
        (BID INTEGER,
        PID INTEGER,
        nazwa varchar(10),
        typ_piwa varchar(10),
        procenty numeric,
        FOREIGN KEY (BID) REFERENCES Browary(BID),
        PRIMARY KEY (PID) )
        ''')
            
