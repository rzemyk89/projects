/*autor: Natalia Rzemieniewska, nr albumu 122472
kod powstał na rzecz pracy magisterskiej "Analiza portfelowa wybranych aktywów na europejskim rynku walutowym z wykorzystaniem programu SAS"*/
PROC IMPORT OUT = nok
DATAFILE = "C:\Users\Rzemyczek\Desktop\MGR\NOKdni.xlsx " /*pobieranie notowań NOK/PLN*/
DBMS = xlsx REPLACE ;
GETNAMES = YES ;
RUN ;
DATA nowe_nok;
SET nok;
L=((W_o-W_z)/W_o)*100; /*tworzenie stóp strat*/
RUN;
TITLE " Rezultaty dla korony norweskiej";
PROC UNIVARIATE data =nowe_nok normal plot;
var L;
histogram L/normal; /*dopasowanie histogramu do rozkładu normalnego*/
RUN ;
DATA stopy_strat_nok ;
SET  nok;
lag_W_O= lag(W_O);
dif_W_O= dif(W_O);
L=-dif(W_O)/lag(W_O); /* tworzenie stóp strat na podstawie funkcji*/
RUN ;
PROC SORT DATA =stopy_strat_nok OUT =stopy_sort_nok;
BY DESCENDING L; /*porządek malejący*/
PROC MEANS DATA =stopy_sort_nok( OBS =3) MEAN ; /*Expected shortfall dla NOK*/
var L;
TITLE " Expected Shortfall dla korony norweskiej ";
RUN ;


PROC IMPORT OUT = frank
DATAFILE = "C:\Users\Rzemyczek\Desktop\MGR\CHFdni.xlsx " /*pobieranie notowań CHF/PLN*/
DBMS = xlsx REPLACE ;
GETNAMES = YES ;
RUN ;
DATA nowe_frank;
SET frank;
L=((W_o-W_z)/W_o)*100; /*tworzenie stóp strat*/
RUN;
TITLE " Rezultaty dla franka szwajcarskiego";
PROC UNIVARIATE data =nowe_frank normal plot;
var L;
histogram L/normal; /*dopasowanie histogramu do rozkładu normalnego*/
RUN ;
DATA stopy_strat_frank ;
SET  frank;
lag_W_O= lag(W_O);
dif_W_O= dif(W_O);
L=-dif(W_O)/lag(W_O);/*tworzenie stóp strat na podstawie funkcji*/
RUN ;
PROC SORT DATA =stopy_strat_frank OUT =stopy_sort_frank;
BY DESCENDING L; /*porządek malejący*/
PROC MEANS DATA =stopy_sort_frank( OBS =3) MEAN ;/*Expected shortfall dla CHF*/
var L;
TITLE " Expected Shortfall dla franka szwajcarskiego ";
RUN ;


PROC IMPORT OUT = euro
DATAFILE = "C:\Users\Rzemyczek\Desktop\MGR\EURdni.xlsx " /*pobieranie notowań EUR/PLN*/
DBMS = xlsx REPLACE ;
GETNAMES = YES ;
RUN ;
DATA nowe_euro;
SET euro;
L=((W_o-W_z)/W_o)*100; /*tworzenie stóp strat*/
RUN;
TITLE " Rezultaty dla euro";
PROC UNIVARIATE data =nowe_euro normal plot;
var L;
histogram L/normal; /*dopasowanie histogramu do rozkładu normalnego*/
RUN ;
DATA stopy_strat_euro ;
SET  euro;
lag_W_O= lag(W_O);
dif_W_O= dif(W_O);
L=-dif(W_O)/lag(W_O); /*tworzenie stóp strat na podstawie funkcji*/
RUN ;
PROC SORT DATA =stopy_strat_euro OUT =stopy_sort_euro;
BY DESCENDING L;/*porządek malejący*/
PROC MEANS DATA =stopy_sort_euro( OBS =3) MEAN ;/*expected shortfall dla EUR*/
var L;
TITLE " Expected Shortfall dla euro ";
RUN ;

title "Test Mardia na normalnosc rozkladu wielowymiarowego"; /*test Mardia*/
proc iml ;
y={
-0.6990500090	0.1174743025	-0.4624783974, /*stopy strat wszystkich walut*/
0.1601993592	0.1587768303	0.0533036125,
-0.1069709396	-0.2208740723	0.0096967346,
-0.3383793411	-0.3966971291	-0.2060755934,
0.1774937877	-0.0790259322	0.1983934966,
0.5334281650	0.9446377914	0.5454545455,
0.3038970325	0.2804842043	0.2145033516,
0.0717231486	-0.1746854182	0.0244277793,
0.3050421676	-0.0384228882	0.0366506218,
0.3959683225	0.1063609773	0.1784317560,
-0.1264907842	-0.1449232498	-0.1493670266,
0.3970402454	0.4636739516	0.4156479218,
0.0000000000	-0.0385722339	0.0368278910,
-0.0905961225	0.1749911021	0.1768390028,
-0.2172338885	-0.1426152063	-0.1304037596,
0.0361271676	-0.3975671265	-0.4988205229,
0.2529815685	0.3368893880	0.3765373237,
-0.5797101449	-0.4240177910	-0.3362375752,
0.0000000000	0.6525333648	0.5161195636,
-0.5043227666	-0.2080423218	-0.2434166851,
-1.4874551971	-1.1537206750	-1.1724307089,
0.4767790924	0.0351844250	0.0727307991,
0.0887154010	-0.0850589547	-0.1091756029,
0.1953471852	0.1406675849	0.1720669850,
0.8718861210	1.0476889215	1.0487473296,
-0.0897504936	0.0741443739	-0.5789990186,
-0.2510760402	0.0563915353	-0.5732266563,
0.1252236136	1.0215596603	0.1940287648,
0.3403188250	-0.3210321032	-0.3134796238,
0.1078360891	0.9211352693	0.2470930233,
0.1439366679	0.1871472124	-0.2817038224,
-0.8108108108	-1.7570387395	-1.1333365622,
-0.1429848079	-0.1218497385	0.1484603228,
-0.2141709798	0.3027694500	0.2613908873,
-0.9260908281	-0.8038824545	-0.8968286408,
0.2293982707	0.5227870160	0.1453626918,
0.6190307747	0.4423990499	0.2338734697,
-0.3915287418	-0.6918970505	-0.3922975721,
0.2481829463	0.3968841631	0.2644809264,
-0.0177714590	-0.7761158524	-0.0286683549,
-0.3731343284	-0.2980230156	-0.3773584906,
1.1683483802	1.2562148804	1.2206148282,
-0.6448146158	-1.0874746752	-0.4456220643,
-0.0355935220	0.2593651449	-0.2422062350,
-0.1245330012	-0.5200791939	-0.1937752685,
-0.0355366027	0.3909809801	0.5061840409,
0.2131438721	0.1800259710	0.3623710103,
-0.6229975080	-0.2542648494	-0.4022254871,
-0.1945869450	0.3568374178	-0.2278942571,
-0.0176553672	-0.1923759915	0.1579665398,
0.1059135040	-0.6144211739	-0.3284190339,
0.1236967662	0.1086286369	-0.1863710217,
0.0530785563	0.2204326358	0.1597901264,
0.7611966720	0.6509764647	0.6234622459,
0.2497324295	-0.6878557875	-0.3797894332,
0.5722460658	0.4799764429	0.4454022989,
-0.3237410072	-0.2396662426	-0.0144320970,
0.1972032987	-0.7881220851	-0.0793650794,
-0.1077779774	-0.1698638161	-0.1081392834,
0.3050421676	0.3830073385	0.2112439387,
0.6119510439	0.6633012444	0.3319701708,
-0.5613908004	0.3220469184	0.1979146553};
n = nrow(y) ; /*liczba wierszy*/
p = ncol(y) ; /*liczba kolumn*/
dfchi = p*(p+1)*(p+2)/6 ; /*współczynnik dfchi*/
q = i(n) - (1/n)*j(n,n,1);
s = (1/(n))*t(y)*q*y ; s_inv = inv(s) ;
g_matrix = q*y*s_inv*t(y)*q;
beta1hat = ( sum(g_matrix#g_matrix#g_matrix) )/(n*n);/*współczynnik beta 1*/
beta2hat =trace( g_matrix#g_matrix )/n ; /*współczynnik beta 2*/
kappa1 = n*beta1hat/6 ; /*współczynnik kappa 1*/
kappa2 = (beta2hat - p*(p+2) ) /sqrt(8*p*(p+2)/n) ; /*współczynnik kappa 2*/
pvalskew = 1 - probchi(kappa1,dfchi) ; /*skośność*/
pvalkurt = 2*( 1 - probnorm(abs(kappa2)) ); /*kurtoza*/
print s ;
print s_inv ;
print "TESTS:";
print "Based on skewness:" beta1hat kappa1 pvalskew ;
print " Based on kurtosis" beta2hat kappa2 pvalkurt;
run;
PROC IMPORT OUT = stopy
DATAFILE = "C:\Users\Rzemyczek\Desktop\MGR\stopy_dni.xlsx " /*pobieranie stóp strat*/
DBMS = xlsx REPLACE ;
GETNAMES = YES ;
RUN ;


proc copula data = stopy; /*funkcja kopuła Claytona*/
title " Clayton ";
var L1 L2 L3;
fit clayton / marginals = EMPIRICAL
plots =( data = BOTH SCATTER )
outcopula = ClWyniki1;
simulate / ndraws = 1000 /*1000 wyników*/
seed = 1234 /*ziarno*/
out = ClGenerowane1
plots =( data = BOTH MATRIX distribution = CDF );
run;
data stopyStrat1;
set ClGenerowane1;
Clayton=1/3*L1+1/3*L2+1/3*L3; /*portfel z udziałam 1/3, 1/3, 1/3*/
run;
data Var;
SET stopyStrat1;
RUN;
proc means data=Var P95; /*VaR Clayton*/
var Clayton;
TITLE ’Value-at-Risk dla Claytona’;
run;
data ES1;
SET stopyStrat1;
RUN;
PROC SORT DATA=ES1 out=ES1_SORT;
BY DESCENDING Clayton;
proc means data=ES1_SORT (OBS=60) mean; /*ES Clayton dla portfela */
var Clayton;
TITLE ’Expected Shortfall dla Claytona’;
run;


proc copula data = stopy; /*funkcja kopuła Frank*/
title " Frank ";
var L1 L2 L3;
fit frank / marginals = EMPIRICAL
plots =( data = BOTH SCATTER )
outcopula = ClWyniki3;
simulate / ndraws = 1000 /*1000 wyników*/
seed = 1234 /*ziarno*/
out = ClGenerowane3
plots =( data = BOTH MATRIX distribution = CDF );
run;
data stopyStrat3;
set ClGenerowane3;
Frank=1/3*L1+1/3*L2+1/3*L3; /*portfel z udziałam 1/3, 1/3, 1/3*/
run;
data Var;
SET stopyStrat3;
RUN;
proc means data=Var P95; /*VaR Frank*/
var Frank;
TITLE ’Value-at-Risk dla Franka’;
run;
data ES3;
SET stopyStrat3;
RUN;
PROC SORT DATA=ES3 out=ES_SORT3;
BY DESCENDING Frank;
proc means data=ES_SORT3 (OBS=60) mean; /*ES Franka dla portfela*/
var Frank;
TITLE ’Expected Shortfall dla Franka’;
run;


proc copula data = stopy; /*funkcja kopuły Gumbel*/
title " Gumbel ";
var L1 L2 L3;
fit gumbel / marginals = EMPIRICAL
plots =( data = BOTH SCATTER )
outcopula = ClWyniki2;
simulate / ndraws = 1000 /*1000 wyników*/
seed = 1234 /*ziarno*/
out = ClGenerowane2
plots =( data = BOTH MATRIX distribution = CDF );
run;
data stopyStrat2;
set ClGenerowane2;
Gumbel=1/3*L1+1/3*L2+1/3*L3; /*portfel z udziałam 1/3, 1/3, 1/3*/
run;
data Var;
SET stopyStrat2;
RUN;
proc means data=Var P95; /*Var Gumbel*/
var Gumbel;
TITLE ’Value-at-Risk dla Gumbela’;
run;
data ES2;
SET stopyStrat2;
RUN;
PROC SORT DATA=ES2 out=ES_SORT2;
BY DESCENDING Gumbel;
proc means data=ES_SORT2 (OBS=60) mean; /*ES Gumbel dla portfela*/
var Gumbel;
TITLE ’Expected Shortfall dla Gumbela’;
run;


proc copula data = stopy; /*funkcja kopuła Normal*/
title " Normal ";
var L1 L2 L3;
fit normal / marginals = EMPIRICAL
plots =( data = BOTH SCATTER )
outcopula = ClWyniki4;
simulate / ndraws = 1000 /*1000 wyników*/
seed = 1234 /*ziarno*/
out = ClGenerowane4
plots =( data = BOTH MATRIX distribution = CDF );
run;
data stopyStrat4;
set ClGenerowane4;
Normal=1/3*L1+1/3*L2+1/3*L3; /*portfel z udziałam 1/3, 1/3, 1/3*/
run;
data Var;
SET stopyStrat4;
RUN;
proc means data=Var P95; /*VaR Normal*/
var Normal;
TITLE ’Value-at-Risk dla Normal’;
run;
data ES4;
SET stopyStrat4;
RUN;
PROC SORT DATA=ES4 out=ES_SORT4;
BY DESCENDING Normal;
proc means data=ES_SORT4 (OBS=60) mean; /*ES Normal dla portfela*/
var Normal;
TITLE ’Expected Shortfall dla Normal’;
run;


proc copula data = stopy; /*funkcja kopuła T*/
title " T ";
var L1 L2 L3;
fit T / marginals = EMPIRICAL
plots =( data = BOTH SCATTER )
outcopula = ClWyniki5;
simulate / ndraws = 1000 /*1000 wyników*/
seed = 1234 /*ziarno*/
out = ClGenerowane5
plots =( data = BOTH MATRIX distribution = CDF );
run;
data stopyStrat5;
set ClGenerowane5;
T=1/3*L1+1/3*L2+1/3*L3; /*portfel z udziałam 1/3, 1/3, 1/3*/
run;
data Var;
SET stopyStrat5;
RUN;
proc means data=Var P95; /*VaR T*/
var T;
TITLE ’Value-at-Risk dla T’;
run;
data ES5;
SET stopyStrat5;
RUN;
PROC SORT DATA=ES5 out=ES_SORT5;
BY DESCENDING T;
proc means data=ES_SORT5 (OBS=60) mean; /*ES T dla portfela*/
var T;
TITLE ’Expected Shortfall dla T’;
run;