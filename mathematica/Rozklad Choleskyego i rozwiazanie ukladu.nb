(************** Content-type: application/mathematica **************

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     12473,        340]*)
(*NotebookOutlinePosition[     13154,        363]*)
(*  CellTagsIndexPosition[     13110,        359]*)
(*WindowFrame->Normal*)



Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
    \(\[IndentingNewLine]\(A = {{6, 3, 2}, {3, 2, 1.5}, {2, 1.5, 
            2}};\)\), "\[IndentingNewLine]", 
    \(\(va = {\(-2\), 0, 2};\)\), "\[IndentingNewLine]", 
    \(Print["\<Macierz A: \>", MatrixForm[A]]\), "\[IndentingNewLine]", 
    \(\(B = {{4, 2, 2}, {2, 5, 3}, {2, 3, 6}};\)\), "\[IndentingNewLine]", 
    \(Print["\<Macierz B: \>", MatrixForm[B]]\), "\[IndentingNewLine]", 
    \(\(vb = {8, 6, 1};\)\), "\[IndentingNewLine]", 
    \(\(H = {{2, 3, 7.5}, {3, 4\/3, 1}, {7.5, 1, 
            0.9}};\)\), "\[IndentingNewLine]", 
    \(\(vh = {0.1, 7, 0.67};\)\), "\[IndentingNewLine]", 
    \(\(rozw[A_, B_, n_, v_] := 
        Module[{z = {}, s, p, x = {}}, \[IndentingNewLine]For[k = 1, 
            k \[LessEqual] n, \(k++\), \[IndentingNewLine]AppendTo[z, 
              0]\[IndentingNewLine]]; \[IndentingNewLine]z[\([1]\)] = 
            v[\([1]\)]/A[\([1, 1]\)]; \[IndentingNewLine]For[i = 2, 
            i \[LessEqual] n, \(i++\), \[IndentingNewLine]s = 
              0; \[IndentingNewLine]For[j = 1, 
              j \[LessEqual] \((i - 1)\), \(j++\), \[IndentingNewLine]s = 
                s + A[\([i, j]\)]*
                    z[\([j]\)]\[IndentingNewLine]]; \
\[IndentingNewLine]z[\([i]\)] = \((v[\([i]\)] - s)\)/
                A[\([i, i]\)]\[IndentingNewLine]]; 
          Print[z]; \
\[IndentingNewLine]\[IndentingNewLine]\[IndentingNewLine]For[k = 1, 
            k \[LessEqual] n, \(k++\), \[IndentingNewLine]AppendTo[x, 
              0]\[IndentingNewLine]]; \[IndentingNewLine]x[\([n]\)] = 
            z[\([n]\)]/B[\([n, n]\)]; \[IndentingNewLine]For[i = n, 
            i \[GreaterEqual] 1, \(i--\), \[IndentingNewLine]p = 
              0; \[IndentingNewLine]For[j = i + 1, 
              j \[LessEqual] n, \(j++\), \[IndentingNewLine]p = 
                p + B[\([i, j]\)]*
                    x[\([j]\)]\[IndentingNewLine]]; \
\[IndentingNewLine]x[\([i]\)] = \((z[\([i]\)] - p)\)/
                B[\([i, 
                    i]\)]\[IndentingNewLine]]; \
\[IndentingNewLine]Print["\<rozwi\:0105zanie \
ukladu:\>"]\[IndentingNewLine]Print[
              x]\[IndentingNewLine]];\)\[IndentingNewLine]\), "\
\[IndentingNewLine]", 
    \(\(Cholesky[n_, M_, vx_] := 
        Module[{L = {}, p, p1, z = {}, 
            U = Table[0, {i, 1, n}, {j, 1, n}]}, \[IndentingNewLine]L = 
            Table[0, {i, 1, n}, {j, 1, n}]; \[IndentingNewLine]For[k = 1, 
            k \[LessEqual] n, \(k++\), \[IndentingNewLine]p = 
              0; \[IndentingNewLine]For[s = 1, 
              s \[LessEqual] k - 1, \(s++\), \[IndentingNewLine]p = 
                p + \((L[\([k, s]\)])\)^2]; \[IndentingNewLine]L[\([k, 
                  k]\)] = \((M[\([k, k]\)] - p)\)^\((1/
                    2)\); \[IndentingNewLine]For[i = k + 1, 
              i \[LessEqual] n, \(i++\), \[IndentingNewLine]p1 = 
                0; \[IndentingNewLine]For[s = 1, 
                s \[LessEqual] k - 1, \(s++\), \[IndentingNewLine]p1 = 
                  p1 + L[\([k, s]\)]*
                      L[\([i, 
                          s]\)]\[IndentingNewLine]]; \
\[IndentingNewLine]L[\([i, k]\)] = \((M[\([i, k]\)] - p1)\)/
                  L[\([k, 
                      k]\)]\[IndentingNewLine]];\[IndentingNewLine]]; \
\[IndentingNewLine]For[i = 1, 
            i \[LessEqual] 
              n, \(i++\), \[IndentingNewLine]\(For[j = 1, 
                j \[LessEqual] n, \(j++\), \[IndentingNewLine]U[\([i, j]\)] = 
                  L[\([j, 
                      i]\)]\[IndentingNewLine]];\)\[IndentingNewLine]]; \
\[IndentingNewLine]\[IndentingNewLine]\ \ Print["\<L= \>", MatrixForm[L]] 
            Print["\<U= \>", \ 
              MatrixForm[
                U]]\[IndentingNewLine]Print["\<rozwi\:0105zanie \
uk\[LSlash]adu Lz=v: \>"]\[IndentingNewLine]rozw[L, U, 3, 
              vx];\[IndentingNewLine]\[IndentingNewLine]];\)\
\[IndentingNewLine]\[IndentingNewLine]\[IndentingNewLine]\), "\
\[IndentingNewLine]", 
    \(Cholesky[3, B, vb]\), "\[IndentingNewLine]", 
    \(Cholesky[3, A, va]\), "\[IndentingNewLine]", 
    \(Cholesky[3, H, vh]\[IndentingNewLine]\), "\[IndentingNewLine]", 
    \(\)}], "Input"],

Cell[BoxData[
    InterpretationBox[
      RowBox[{"\<\"Macierz A: \"\>", "\[InvisibleSpace]", 
        TagBox[
          RowBox[{"(", "\[NoBreak]", GridBox[{
                {"6", "3", "2"},
                {"3", "2", "1.5`"},
                {"2", "1.5`", "2"}
                }], "\[NoBreak]", ")"}],
          Function[ BoxForm`e$, 
            MatrixForm[ BoxForm`e$]]]}],
      SequenceForm[ "Macierz A: ", 
        MatrixForm[ {{6, 3, 2}, {3, 2, 1.5}, {2, 1.5, 2}}]],
      Editable->False]], "Print"],

Cell[BoxData[
    InterpretationBox[
      RowBox[{"\<\"Macierz B: \"\>", "\[InvisibleSpace]", 
        TagBox[
          RowBox[{"(", "\[NoBreak]", GridBox[{
                {"4", "2", "2"},
                {"2", "5", "3"},
                {"2", "3", "6"}
                }], "\[NoBreak]", ")"}],
          Function[ BoxForm`e$, 
            MatrixForm[ BoxForm`e$]]]}],
      SequenceForm[ "Macierz B: ", 
        MatrixForm[ {{4, 2, 2}, {2, 5, 3}, {2, 3, 6}}]],
      Editable->False]], "Print"],

Cell[BoxData[
    \({0.1`, 7, 0.67`}\)], "Output"],

Cell[BoxData[
    InterpretationBox[
      RowBox[{"\<\"L= \"\>", "\[InvisibleSpace]", 
        TagBox[
          RowBox[{"(", "\[NoBreak]", GridBox[{
                {"2", "0", "0"},
                {"1", "2", "0"},
                {"1", "1", "2"}
                }], "\[NoBreak]", ")"}],
          Function[ BoxForm`e$, 
            MatrixForm[ BoxForm`e$]]]}],
      SequenceForm[ "L= ", 
        MatrixForm[ {{2, 0, 0}, {1, 2, 0}, {1, 1, 2}}]],
      Editable->False]], "Print"],

Cell[BoxData[
    InterpretationBox[
      RowBox[{"\<\"U= \"\>", "\[InvisibleSpace]", 
        TagBox[
          RowBox[{"(", "\[NoBreak]", GridBox[{
                {"2", "1", "1"},
                {"0", "2", "1"},
                {"0", "0", "2"}
                }], "\[NoBreak]", ")"}],
          Function[ BoxForm`e$, 
            MatrixForm[ BoxForm`e$]]]}],
      SequenceForm[ "U= ", 
        MatrixForm[ {{2, 1, 1}, {0, 2, 1}, {0, 0, 2}}]],
      Editable->False]], "Print"],

Cell[BoxData[
    \("rozwi\:0105zanie uk\[LSlash]adu Lz=v: "\)], "Print"],

Cell[BoxData[
    \({4, 1, \(-2\)}\)], "Print"],

Cell[BoxData[
    \("rozwi\:0105zanie ukladu:"\)], "Print"],

Cell[BoxData[
    \({2, 1, \(-1\)}\)], "Print"],

Cell[BoxData[
    InterpretationBox[
      RowBox[{"\<\"L= \"\>", "\[InvisibleSpace]", 
        TagBox[
          RowBox[{"(", "\[NoBreak]", GridBox[{
                {\(\@6\), "0", "0"},
                {\(\@\(3\/2\)\), \(1\/\@2\), "0"},
                {\(\@\(2\/3\)\), "0.7071067811865476`", 
                  "0.9128709291752768`"}
                }], "\[NoBreak]", ")"}],
          Function[ BoxForm`e$, 
            MatrixForm[ BoxForm`e$]]]}],
      SequenceForm[ "L= ", 
        MatrixForm[ {{
          Power[ 6, 
            Rational[ 1, 2]], 0, 0}, {
          Power[ 
            Rational[ 3, 2], 
            Rational[ 1, 2]], 
          Power[ 2, 
            Rational[ -1, 2]], 0}, {
          Power[ 
            Rational[ 2, 3], 
            Rational[ 1, 2]], .70710678118654757, .91287092917527679}}]],
      Editable->False]], "Print"],

Cell[BoxData[
    InterpretationBox[
      RowBox[{"\<\"U= \"\>", "\[InvisibleSpace]", 
        TagBox[
          RowBox[{"(", "\[NoBreak]", GridBox[{
                {\(\@6\), \(\@\(3\/2\)\), \(\@\(2\/3\)\)},
                {"0", \(1\/\@2\), "0.7071067811865476`"},
                {"0", "0", "0.9128709291752768`"}
                }], "\[NoBreak]", ")"}],
          Function[ BoxForm`e$, 
            MatrixForm[ BoxForm`e$]]]}],
      SequenceForm[ "U= ", 
        MatrixForm[ {{
          Power[ 6, 
            Rational[ 1, 2]], 
          Power[ 
            Rational[ 3, 2], 
            Rational[ 1, 2]], 
          Power[ 
            Rational[ 2, 3], 
            Rational[ 1, 2]]}, {0, 
          Power[ 2, 
            Rational[ -1, 2]], .70710678118654757}, {0, 
          0, .91287092917527679}}]],
      Editable->False]], "Print"],

Cell[BoxData[
    \("rozwi\:0105zanie uk\[LSlash]adu Lz=v: "\)], "Print"],

Cell[BoxData[
    \({\(-\@\(2\/3\)\), \@2, 1.8257418583505538`}\)], "Print"],

Cell[BoxData[
    \("rozwi\:0105zanie ukladu:"\)], "Print"],

Cell[BoxData[
    \({\(-0.9999999999999999`\), \(-3.1401849173675503`*^-16\), 
      2.0000000000000004`}\)], "Print"],

Cell[BoxData[
    InterpretationBox[
      RowBox[{"\<\"L= \"\>", "\[InvisibleSpace]", 
        TagBox[
          RowBox[{"(", "\[NoBreak]", GridBox[{
                {\(\@2\), "0", "0"},
                {\(3\/\@2\), \(\[ImaginaryI]\ \@\(19\/6\)\), "0"},
                {"5.303300858899107`", \(5.760002741227418`\ \[ImaginaryI]\), 
                  "2.439801544992413`"}
                }], "\[NoBreak]", ")"}],
          Function[ BoxForm`e$, 
            MatrixForm[ BoxForm`e$]]]}],
      SequenceForm[ "L= ", 
        MatrixForm[ {{
          Power[ 2, 
            Rational[ 1, 2]], 0, 0}, {
          Times[ 3, 
            Power[ 2, 
              Rational[ -1, 2]]], 
          Times[ 
            Complex[ 0, 1], 
            Power[ 
              Rational[ 19, 6], 
              Rational[ 1, 2]]], 0}, {5.3033008588991066, 
          Complex[ 0, 5.7600027412274182], 2.4398015449924131}}]],
      Editable->False]], "Print"],

Cell[BoxData[
    InterpretationBox[
      RowBox[{"\<\"U= \"\>", "\[InvisibleSpace]", 
        TagBox[
          RowBox[{"(", "\[NoBreak]", GridBox[{
                {\(\@2\), \(3\/\@2\), "5.303300858899107`"},
                {
                  "0", \(\[ImaginaryI]\ \@\(19\/6\)\), \(5.760002741227418`\ \
\[ImaginaryI]\)},
                {"0", "0", "2.439801544992413`"}
                }], "\[NoBreak]", ")"}],
          Function[ BoxForm`e$, 
            MatrixForm[ BoxForm`e$]]]}],
      SequenceForm[ "U= ", 
        MatrixForm[ {{
          Power[ 2, 
            Rational[ 1, 2]], 
          Times[ 3, 
            Power[ 2, 
              Rational[ -1, 2]]], 5.3033008588991066}, {0, 
          Times[ 
            Complex[ 0, 1], 
            Power[ 
              Rational[ 19, 6], 
              Rational[ 1, 2]]], 
          Complex[ 0, 5.7600027412274182]}, {0, 0, 2.4398015449924131}}]],
      Editable->False]], "Print"],

Cell[BoxData[
    \("rozwi\:0105zanie uk\[LSlash]adu Lz=v: "\)], "Print"],

Cell[BoxData[
    \({0.07071067811865477`, \(-3.8493676856007615`\)\ \[ImaginaryI], \
\(-8.966863909876185`\)}\)], "Print"],

Cell[BoxData[
    \("rozwi\:0105zanie ukladu:"\)], "Print"],

Cell[BoxData[
    \({\(-0.7673740053050395`\), 
      9.733023872679047`, \(-3.6752431476569414`\)}\)], "Print"]
}, Open  ]]
},
FrontEndVersion->"4.1 for Microsoft Windows",
ScreenRectangle->{{0, 1280}, {0, 717}},
WindowSize->{487, 561},
WindowMargins->{{318, Automatic}, {Automatic, 0}},
StyleDefinitions -> "Classroom.nb"
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{

Cell[CellGroupData[{
Cell[1727, 52, 4148, 76, 1564, "Input"],
Cell[5878, 130, 508, 13, 53, "Print"],
Cell[6389, 145, 498, 13, 53, "Print"],
Cell[6890, 160, 50, 1, 49, "Output"],
Cell[6943, 163, 482, 13, 53, "Print"],
Cell[7428, 178, 482, 13, 53, "Print"],
Cell[7913, 193, 73, 1, 25, "Print"],
Cell[7989, 196, 47, 1, 25, "Print"],
Cell[8039, 199, 59, 1, 25, "Print"],
Cell[8101, 202, 47, 1, 25, "Print"],
Cell[8151, 205, 855, 24, 111, "Print"],
Cell[9009, 231, 847, 24, 95, "Print"],
Cell[9859, 257, 73, 1, 25, "Print"],
Cell[9935, 260, 76, 1, 47, "Print"],
Cell[10014, 263, 59, 1, 25, "Print"],
Cell[10076, 266, 118, 2, 25, "Print"],
Cell[10197, 270, 938, 25, 85, "Print"],
Cell[11138, 297, 940, 26, 97, "Print"],
Cell[12081, 325, 73, 1, 25, "Print"],
Cell[12157, 328, 123, 2, 25, "Print"],
Cell[12283, 332, 59, 1, 25, "Print"],
Cell[12345, 335, 112, 2, 25, "Print"]
}, Open  ]]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

